# Exercise documentation
## Build
```
    ./mvnw clean package
```
## Run
- using spring boot maven plugin:
```
./mvnw spring-boot:run
```
- using created in build process jar file:
```
java -jar ./target/exercise-0.0.1-SNAPSHOT.jar
```
## Usage

### Endpoint 1.
```/api/fda/drugs?{manufacturer-name=?}&{brand-name=?}&{page=?}&{size=?}``` for searching in Open FDA API


#### Examples
- find all

```GET http://localhost:8080/api/fda/drugs```

- find by manufacturer name:

```GET http://localhost:8080/api/fda/drugs?manufacturer-name=Perrigo%20New%20York%20Inc```

- find by brand name:

```GET http://localhost:8080/api/fda/drugs?brand-name=RAMIPRIL```

- find by manufacturer name and brand name:

```GET http://localhost:8080/api/fda/drugs?manufacturer-name=Merck&brand-name=SINEMET```

- pagination (ex. page number 2, 5 elements in the page):

```GET http://localhost:8080/api/fda/drugs?page=2&size=5```

### Endpoint 2.
```/api/drugs&{page=?}&{size=?}``` for searching in internal database

#### Examples:
- find all

```GET http://localhost:8080/api/drugs```

- find by id
  
```GET http://localhost:8080/api/drugs/{id}```

- create drug application

```
    POST http://localhost:8080/api/drugs
    Content-Type: application/json

    {
        "manufacturerName": "m-name",
        "substanceName": "s-name",
        "products": [
            {
                "number": "P-123"
            }
        ]
    }
```