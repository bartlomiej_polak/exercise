package pl.bpl.exercise.model;

import javax.persistence.Embeddable;

/*
 * No @Entity here - product seems to be basic or embedded
 */
@Embeddable
public class Product {

    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
