package pl.bpl.exercise.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class SearchParameterBuilder {

    private String brandNameParameter;

    private String manufacturerNameParameter;

    public SearchParameterBuilder(@Value("${exercise.fda.api.brand.name.parameter}") String brandNameParameter,
                                  @Value("${exercise.fda.api.manufacturer.name.parameter}") String manufacturerNameParameter) {
        this.brandNameParameter = brandNameParameter;
        this.manufacturerNameParameter = manufacturerNameParameter;
    }

    public String buildSearchParameterForBothFields(String brandName, String manufacturerName) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!ObjectUtils.isEmpty(brandName)) {
            stringBuilder.append(brandNameParameter)
                         .append(":")
                         .append(brandName);
        }
        if (!ObjectUtils.isEmpty(manufacturerName)) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("+AND+");
            }
            stringBuilder.append(manufacturerNameParameter)
                         .append(":")
                         .append(manufacturerName);
        }
        return stringBuilder.toString();
    }

}
