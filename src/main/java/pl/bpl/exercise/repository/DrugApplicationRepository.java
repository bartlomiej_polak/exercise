package pl.bpl.exercise.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pl.bpl.exercise.model.DrugApplication;

public interface DrugApplicationRepository extends PagingAndSortingRepository<DrugApplication, Long> {
}
