package pl.bpl.exercise.config;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.bpl.exercise.service.OpenFdaRestClientService;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class RestClientConfiguration {

    private String fdaApiServerUrl;

    public RestClientConfiguration(@Value("${exercise.fda.api.server.url}") String fdaApiServerUrl) {
        this.fdaApiServerUrl = fdaApiServerUrl;
    }

    @Bean
    public Retrofit retrofit() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(fdaApiServerUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build())
                .build();
        return retrofit;
    }

    @Bean
    public OpenFdaRestClientService openFdaRestClient(Retrofit retrofit) {
        return retrofit.create(OpenFdaRestClientService.class);
    }
}

