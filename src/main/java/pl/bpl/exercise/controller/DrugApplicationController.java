package pl.bpl.exercise.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.bpl.exercise.model.DrugApplication;
import pl.bpl.exercise.repository.DrugApplicationRepository;

@RestController
@RequestMapping("/api/drugs")
public class DrugApplicationController {

    private DrugApplicationRepository drugApplicationRepository;

    public DrugApplicationController(DrugApplicationRepository drugApplicationRepository) {
        this.drugApplicationRepository = drugApplicationRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<DrugApplication> findAll(Pageable pageable) {
        return drugApplicationRepository.findAll(pageable);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping("/{id}")
    public ResponseEntity<DrugApplication> findById(@PathVariable Long id) {
        return drugApplicationRepository.findById(id)
                                        .map(ResponseEntity::ok)
                                        .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<DrugApplication> create(@RequestBody DrugApplication drugApplication) {
        return new ResponseEntity(drugApplicationRepository.save(drugApplication), HttpStatus.CREATED);
    }
}
