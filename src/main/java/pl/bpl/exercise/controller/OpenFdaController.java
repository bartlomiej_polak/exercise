package pl.bpl.exercise.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.bpl.exercise.service.OpenFdaService;

@RestController
@RequestMapping("/api/fda/drugs")
public class OpenFdaController {

    private OpenFdaService openFdaService;

    public OpenFdaController(OpenFdaService openFdaService) {
        this.openFdaService = openFdaService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<JsonNode> search(@RequestParam(value = "brand-name", required = false) String brandName,
                                 @RequestParam(value = "manufacturer-name", required = false) String manufacturerName,
                                 Pageable pageable) {
        return openFdaService.findApplications(brandName, manufacturerName, pageable);
    }
}
