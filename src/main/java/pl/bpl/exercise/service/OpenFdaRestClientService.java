package pl.bpl.exercise.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenFdaRestClientService {

    @GET("drug/drugsfda.json")
    Call<ResponseBody> searchRecords(@Query(value = "search", encoded = true) String search, @Query("skip") Long skip,
                                     @Query("limit") Integer limit);
}
