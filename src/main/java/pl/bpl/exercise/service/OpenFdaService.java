package pl.bpl.exercise.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.bpl.exercise.component.SearchParameterBuilder;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OpenFdaService {

    public static final Logger log = LoggerFactory.getLogger(OpenFdaService.class);

    private OpenFdaRestClientService openFdaRestClientService;

    private SearchParameterBuilder searchParameterBuilder;

    private ObjectMapper objectMapper;

    public OpenFdaService(OpenFdaRestClientService openFdaRestClientService,
                          SearchParameterBuilder searchParameterBuilder, ObjectMapper objectMapper) {
        this.openFdaRestClientService = openFdaRestClientService;
        this.searchParameterBuilder = searchParameterBuilder;
        this.objectMapper = objectMapper;
    }

    public Page<JsonNode> findApplications(String brandName, String manufacturerName, Pageable pageable) {
        Response<ResponseBody> response;
        Integer limit = pageable.getPageSize();
        Long skip = pageable.getOffset();
        try {
            String searchParameter = searchParameterBuilder.buildSearchParameterForBothFields(brandName, manufacturerName);
            Call<ResponseBody> call = openFdaRestClientService.searchRecords(searchParameter, skip, limit);
            log.debug("Executing request url='{}'", call.request().url());
            response = call.execute();
            log.debug("Response code='{}', message='{}'", response.code(), response.message());
            return response.isSuccessful() ? extractPage(response.body(), pageable)
                                           : throwResponseStatusException(response);
        } catch (IOException e) {
            String message = "Error during executing findApplications method";
            log.error(message, e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message, e);
        }
    }

    private Page<JsonNode> throwResponseStatusException(Response<ResponseBody> response) throws IOException {
        throw new ResponseStatusException(HttpStatus.resolve(response.code()), response.errorBody().string());
    }

    private Page<JsonNode> extractPage(ResponseBody responseBody, Pageable pageable) throws IOException {
        JsonNode body = extractBody(responseBody.string());
        return new PageImpl<>(extractResults(body), pageable, extractResultsSize(body));
    }

    private JsonNode extractBody(String stringBody) throws JsonProcessingException {
        return objectMapper.readTree(stringBody);
    }

    private List<JsonNode> extractResults(JsonNode jsonNode) {
        return StreamSupport.stream(jsonNode.get("results").spliterator(), false)
                            .collect(Collectors.toList());
    }

    private long extractResultsSize(JsonNode jsonNode) {
        return jsonNode.get("meta")
                       .get("results")
                       .get("total")
                       .asLong();
    }
}
