CREATE TABLE drug_application
(
    application_id      bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    manufacturer_name   varchar(255) NOT NULL,
    substance_name      varchar(255) NOT NULL
);

CREATE INDEX drug_application_manufacturer_name_ix ON drug_application (manufacturer_name);
CREATE INDEX drug_application_substance_name_ix ON drug_application (substance_name);

CREATE TABLE product
(
    product_id          bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    product_number      varchar(100) NOT NULL,
    application_id      bigint
);

ALTER TABLE product ADD FOREIGN KEY (application_id) REFERENCES drug_application(application_id);