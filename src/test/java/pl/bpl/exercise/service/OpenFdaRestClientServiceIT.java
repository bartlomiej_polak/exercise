package pl.bpl.exercise.service;

import okhttp3.ResponseBody;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import retrofit2.Response;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class OpenFdaRestClientServiceIT {

    @Autowired
    private OpenFdaRestClientService openFdaRestClientService;

    @Test
    public void shouldTestConnectionToOpenFda() throws IOException {
        // when
        Response<ResponseBody> response = openFdaRestClientService.searchRecords(null, null, null)
                                                                  .execute();

        // then
        assertThat(response.message()).isEqualTo("OK");
        assertThat(response.isSuccessful()).isTrue();
        assertThat(response.code()).isEqualTo(200);
    }

    @Test
    public void shouldTestConnectionToOpenFdaWhenNoResultsFound() throws IOException {
        // when
        Response<ResponseBody> response = openFdaRestClientService.searchRecords("notfound", null, null)
                                                                  .execute();

        // then
        assertThat(response.message()).isEqualTo("Not Found");
        assertThat(response.isSuccessful()).isFalse();
        assertThat(response.code()).isEqualTo(404);
        assertThat(response.errorBody()).isNotNull();
    }
}