package pl.bpl.exercise.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Request;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.server.ResponseStatusException;
import pl.bpl.exercise.component.SearchParameterBuilder;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OpenFdaServiceTest {

    private OpenFdaService openFdaService;
    private SearchParameterBuilder searchParameterBuilder;
    private OpenFdaRestClientService openFdaRestClientService;
    private ResponseBody responseBody;
    private Call<ResponseBody> call;
    private Response<ResponseBody> response;
    private Pageable pageable;
    private Request request;
    private String brandName = "b-name";
    private String manufacturerName = "m-name";

    @BeforeEach
    public void setup() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        openFdaRestClientService = mock(OpenFdaRestClientService.class);
        searchParameterBuilder = mock(SearchParameterBuilder.class);
        openFdaService = new OpenFdaService(openFdaRestClientService, searchParameterBuilder, objectMapper);

        responseBody = mock(ResponseBody.class);
        response = mock(Response.class);
        pageable = mock(Pageable.class);
        request = mock(Request.class);
        call = mock(Call.class);

        when(response.body()).thenReturn(responseBody);
        when(response.errorBody()).thenReturn(responseBody);
        when(call.execute()).thenReturn(response);
        when(call.request()).thenReturn(request);
        when(openFdaRestClientService.searchRecords(any(), any(), any())).thenReturn(call);
    }

    @Test
    public void shouldCreatePageForSuccessfulResponse() throws IOException {
        // mock
        when(responseBody.string()).thenReturn(readSuccessfulResponse());
        when(response.isSuccessful()).thenReturn(true);

        // when
        Page<JsonNode> result = openFdaService.findApplications(brandName, manufacturerName, pageable);

        // then
        assertThat(result.getTotalElements()).isEqualTo(1L);
        assertThat(result.getTotalPages()).isEqualTo(1L);
        assertThat(result.hasContent()).isTrue();
    }

    @Test
    public void shouldNotCreatePageForErrorResponse() throws IOException {
        // mock
        when(responseBody.string()).thenReturn(readErrorResponse());
        when(response.isSuccessful()).thenReturn(false);
        when(response.code()).thenReturn(404);

        // then
        assertThatCode(() -> openFdaService.findApplications(brandName, manufacturerName, pageable))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining("No matches found!");
    }

    private String readSuccessfulResponse() throws IOException {
        return Files.readString(Path.of("src/test/resources/test-response.json"));
    }

    private String readErrorResponse() throws IOException {
        return Files.readString(Path.of("src/test/resources/test-error-response.json"));
    }
}