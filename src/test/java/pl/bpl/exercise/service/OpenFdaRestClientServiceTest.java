package pl.bpl.exercise.service;

import okhttp3.Request;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import retrofit2.Call;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class OpenFdaRestClientServiceTest {

    @Autowired
    private OpenFdaRestClientService openFdaRestClientService;

    @Test
    public void shouldPrepareValidRequestWithNoParameters() {
        // given
        Call<ResponseBody> call = openFdaRestClientService.searchRecords(null, null, null);

        // when
        Request request = call.request();

        // then
        assertThat(request.method()).isEqualTo("GET");
        assertThat(request.isHttps()).isTrue();
        assertThat(request.url().host()).isEqualTo("api.fda.gov");
        assertThat(request.url().pathSegments()).containsExactly("drug", "drugsfda.json");
        assertThat(request.url().queryParameter("search")).isNull();
        assertThat(request.url().queryParameter("skip")).isNull();
        assertThat(request.url().queryParameter("limit")).isNull();
    }

    @Test
    public void shouldPrepareValidRequestWithParameters() {
        // given
        Call<ResponseBody> call = openFdaRestClientService.searchRecords("query term", 10L, 20);

        // when
        Request request = call.request();

        // then
        assertThat(request.method()).isEqualTo("GET");
        assertThat(request.isHttps()).isTrue();
        assertThat(request.url().host()).isEqualTo("api.fda.gov");
        assertThat(request.url().pathSegments()).containsExactly("drug", "drugsfda.json");
        assertThat(request.url().queryParameter("search")).isEqualTo("query term");
        assertThat(request.url().queryParameter("skip")).isEqualTo("10");
        assertThat(request.url().queryParameter("limit")).isEqualTo("20");
    }
}