package pl.bpl.exercise.component;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SearchParameterBuilderTest {

    private SearchParameterBuilder searchParameterBuilder = new SearchParameterBuilder("brand-param", "manufacturer-param");

    @ParameterizedTest
    @MethodSource("provideValuesForCreateValidSearchParameter")
    public void shouldCreateValidSearchParameter(String brandName, String manufacturerName, String expectedParameter) {
        // when
        String searchParameter = searchParameterBuilder.buildSearchParameterForBothFields(brandName, manufacturerName);

        // then
        assertThat(searchParameter).isEqualTo(expectedParameter);
    }

    private static Stream<Arguments> provideValuesForCreateValidSearchParameter() {
        return Stream.of(
                Arguments.of(null, null, ""),
                Arguments.of("", "", ""),
                Arguments.of("b-name", null, "brand-param:b-name"),
                Arguments.of(null, "m-name", "manufacturer-param:m-name"),
                Arguments.of("b-name", "m-name", "brand-param:b-name+AND+manufacturer-param:m-name")
        );
    }
}