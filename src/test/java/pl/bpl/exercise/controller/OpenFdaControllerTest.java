package pl.bpl.exercise.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.server.ResponseStatusException;
import pl.bpl.exercise.service.OpenFdaService;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = OpenFdaController.class)
class OpenFdaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OpenFdaService openFdaService;

    @Test
    public void shouldReturnValidResponseForEmptyResult() throws Exception {
        // mock
        Mockito.when(openFdaService.findApplications(any(), any(), any()))
               .thenReturn(new PageImpl<>(Collections.emptyList(), Pageable.unpaged(), 0));

        // when & then
        mockMvc.perform(get("/api/fda/drugs").contentType("application/json"))
                                  .andExpect(status().isOk())
                                  .andExpect(MockMvcResultMatchers.jsonPath("$.empty").value(true))
                                  .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(0));
    }

    @Test
    public void shouldReturnValidResponseForValidResultPage() throws Exception {
        // mock
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree("{\"k1\":\"v1\"}");

        Mockito.when(openFdaService.findApplications(any(), any(), any()))
               .thenReturn(new PageImpl<>(List.of(jsonNode), Pageable.unpaged(), 1));

        // when & then
        mockMvc.perform(get("/api/fda/drugs").contentType("application/json"))
               .andExpect(status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.empty").value(false))
               .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1));
    }

    @Test
    public void shouldReturnValidResponseWhenInternalErrorOccurs() throws Exception {
        // mock
        Mockito.when(openFdaService.findApplications(any(), any(), any()))
               .thenThrow(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "message-test"));

        // when & then
        mockMvc.perform(get("/api/fda/drugs").contentType("application/json"))
               .andExpect(status().isInternalServerError())
               .andExpect(status().reason("message-test"));
    }

    @Test
    public void shouldReturnValidResponseWhenResponseStatusExceptionOccurs() throws Exception {
        // mock
        Mockito.when(openFdaService.findApplications(any(), any(), any()))
               .thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "message-test"));

        // when & then
        mockMvc.perform(get("/api/fda/drugs").contentType("application/json"))
               .andExpect(status().isNotFound())
               .andExpect(status().reason("message-test"));
    }
}