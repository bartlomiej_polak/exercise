package pl.bpl.exercise.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import pl.bpl.exercise.model.DrugApplication;
import pl.bpl.exercise.model.Product;
import pl.bpl.exercise.repository.DrugApplicationRepository;

import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = DrugApplicationController.class)
class DrugApplicationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DrugApplicationRepository drugApplicationRepository;

    @Test
    public void shouldReturnValidResponseWhenNewEntityCreated() throws Exception {
        // given
        DrugApplication drugApplication = createTestDrugApplication(12L);

        // mock
        Mockito.when(drugApplicationRepository.save(any()))
               .thenReturn(drugApplication);

        // when & then
        mockMvc.perform(post("/api/drugs", drugApplication)
                .contentType("application/json")
                .content("{ \"manufacturerName\": \"aaa\", \"substanceName\": \"bbb\", \"products\": []}"))
               .andExpect(status().isCreated())
               .andExpect(content().string("{\"id\":12,\"manufacturerName\":\"m-name\",\"substanceName\":\"s-name\",\"products\":[{\"number\":\"P-123\"}]}"));
    }

    @Test
    public void shouldReturnValidResponseWhenFindByIdPerformed() throws Exception {
        // given
        DrugApplication drugApplication = createTestDrugApplication(9L);

        // mock
        Mockito.when(drugApplicationRepository.findById(any()))
               .thenReturn(Optional.of(drugApplication));

        // when & then
        mockMvc.perform(get("/api/drugs/9"))
               .andExpect(status().isOk())
               .andExpect(content().string("{\"id\":9,\"manufacturerName\":\"m-name\",\"substanceName\":\"s-name\",\"products\":[{\"number\":\"P-123\"}]}"));
    }

    @Test
    public void shouldReturnValidResponseWhenFindByIdPerformedAndNotFound() throws Exception {
        // mock
        Mockito.when(drugApplicationRepository.findById(any()))
               .thenReturn(Optional.empty());

        // when & then
        mockMvc.perform(get("/api/drugs/9"))
               .andExpect(status().isNotFound());
    }

    private DrugApplication createTestDrugApplication(Long id) {
        DrugApplication drugApplication = new DrugApplication();
        drugApplication.setId(id);
        drugApplication.setManufacturerName("m-name");
        drugApplication.setSubstanceName("s-name");
        Product product = new Product();
        product.setNumber("P-123");
        drugApplication.setProducts(Set.of(product));
        return drugApplication;
    }

}